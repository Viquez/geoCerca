//
//  ViewController.swift
//  ParkillerDemo
//
//  Created by Alejandro Viquez on 02/09/16.
//  Copyright © 2016 Alejandro Viquez. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Social
import Accounts

extension UIView {
    
    func pb_takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.mainScreen().scale)
        
        drawViewHierarchyInRect(self.bounds, afterScreenUpdates: true)
        
        // old style: layer.renderInContext(UIGraphicsGetCurrentContext())
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

class ViewController: UIViewController, CLLocationManagerDelegate, LocateOnTheMap, UISearchBarDelegate {
    let notification = UILocalNotification()
    
    
    var startLocation: CLLocation!
    var circ : GMSCircle!
    var circ2 : GMSCircle!
    var circ3 : GMSCircle!
    var didYouPutMarker: Bool = false
    var locationManager = CLLocationManager()
    var locationOk: CLLocationCoordinate2D!
    var geocode: GMSGeocoder!
    var searchResultController: SearchResultsController!
    var resultsArray = [String]()
    var marker2: GMSMarker!
    var didFindMyLocation = false
    var marker: GMSMarker!
    var distance: CLLocationDistance!
    var locationOut: CLLocation!
    var locationMe: CLLocation!
    //var startLocation : CLLocation!
    var endLocation :CLLocation!
    var DistanceInt: Int = 0
    var FlagToGeoNear: Int = 0
    var nearTo10: Bool = false
    var nearTo50: Bool = false
    var nearTo200: Bool = false
    var nearTo100: Bool = false
    var near: Bool = false
    var stateAplication: Bool = true
    
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelDirection: UILabel!
    
    @IBOutlet weak var myMap: GMSMapView!
    
    
    @IBAction func startSensing(sender: AnyObject) {
        if locationOut == nil {
        
            print("Primero selecciona un destino")
            let alert = UIAlertController(title: "Alerta", message: "Debes seleccionar un destino antes de iniciar", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        
        }
        else {
            locationManager.startUpdatingLocation()
            self.didYouPutMarker = true
            
            print("Bandera activada")
            let alert = UIAlertController(title: "Maps", message: "Inicio Correctamente la medición", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)

        
        }

        
        
    }
    
    func publishOnTwitter() {
        
        
        
        let screenshot = self.view?.pb_takeSnapshot()
        //Aqui meter lo de la lat y long:
        
        if(SLComposeViewController.isAvailableForServiceType(SLServiceTypeTwitter)) {
            let socialController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            socialController.setInitialText("Estoy a 10 metros de mi destino! \(locationOk.latitude) , \(locationOk.longitude)")
            socialController.addImage(screenshot)
            
            self.presentViewController(socialController, animated: true, completion: nil)
        }
    
    }
   
    
    @IBAction func restartAll(sender: AnyObject) {
        
        marker = nil
        //marker2.map = nil
        didYouPutMarker = false
        distance = nil
        
        
        if locationOut != nil {
            
            locationOut = nil
            marker2.map = nil
            self.circ3.map = nil
            self.circ2.map = nil
            self.circ.map = nil
            labelDistance.text = ""
            labelDirection.text = ""
         

        }
         
        
        
        
    }
   

    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
       locationManager.allowsBackgroundLocationUpdates = true
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters

        myMap.mapType = kGMSTypeNormal
        let camera: GMSCameraPosition = GMSCameraPosition.cameraWithLatitude(48.857165, longitude: 2.354613, zoom: 8.0)
        startLocation = nil
        
        myMap.camera = camera
        //print(location.altitude)
        
        myMap.myLocationEnabled = true

        myMap.padding = UIEdgeInsets(top: self.topLayoutGuide.length, left: 0,
                                            bottom: 40, right: 0)
        myMap.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.New, context: nil)
        
            // Do any additional setup after loading the view, typically from a nib.
    }
    
    
        
        
        
    
    
    override func viewDidAppear(animated: Bool) {
        
        
        

        searchResultController = SearchResultsController()
        searchResultController.delegate = self
        

    }
    
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        let placeClient = GMSPlacesClient()
        placeClient.autocompleteQuery(searchText, bounds: nil, filter: nil) { (results, error: NSError?) -> Void in
            
            self.resultsArray.removeAll()
            if results == nil {
                return
            }
            
            for result in results! {
                if let result = result as? GMSAutocompletePrediction {
                    self.resultsArray.append(result.attributedFullText.string)
                }
            }
            
            self.searchResultController.reloadDataWithArray(self.resultsArray)
            
        }
    }
    
    

    func locateWithLongitude(lon: Double, andLatitude lat: Double, andTitle title: String) {
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            //Here
            
            
            
            
            let position = CLLocationCoordinate2DMake(lat, lon)
            self.marker2 = GMSMarker(position: position)
            
            let camera = GMSCameraPosition.cameraWithLatitude(lat, longitude: lon, zoom: 15)
            self.myMap.camera = camera
            
            self.marker2.title = "\(title)"
            self.marker2.appearAnimation = kGMSMarkerAnimationPop
            self.marker2.icon = GMSMarker.markerImageWithColor(UIColor.grayColor())
            self.labelDirection.text = title
            self.marker2.map = self.myMap
            
            let circleCenter = position
            self.circ = GMSCircle(position: circleCenter, radius: 10)
            self.circ2 = GMSCircle(position: circleCenter, radius: 100)
            self.circ3 = GMSCircle(position: circleCenter, radius: 200)
            self.circ.strokeColor = UIColor.clearColor()
            self.circ2.strokeColor = UIColor.clearColor()
            self.circ3.strokeColor = UIColor.clearColor()
            self.circ.fillColor = UIColor.blueColor()
            self.circ2.fillColor = UIColor.redColor()
            self.circ3.fillColor = UIColor.cyanColor()
            
            
            self.circ3.map = self.myMap
            self.circ2.map = self.myMap
            self.circ.map = self.myMap
            let latitude: CLLocationDegrees = position.latitude
            let longitude: CLLocationDegrees = position.longitude
            
            let locationNear: CLLocation = CLLocation(latitude: latitude,
                                                  longitude: longitude)
            self.locationOut = locationNear
            print(self.locationOut.coordinate.latitude.description)
            print(self.locationOut.coordinate.longitude.description)
            
            let distance: CLLocationDistance = self.locationMe.distanceFromLocation(self.locationOut)
            //print("La distancia en metros es de \(distance.description)")
            

        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func addressFinder(sender: AnyObject) {
        
        let searchController = UISearchController(searchResultsController: searchResultController)
        searchController.searchBar.delegate = self
        self.presentViewController(searchController, animated: true, completion: nil)
    }
    
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if !didFindMyLocation {
            
            let myLocation: CLLocation = change![NSKeyValueChangeNewKey] as! CLLocation
            myMap.camera = GMSCameraPosition.cameraWithTarget(myLocation.coordinate, zoom: 15.0)
            locationOk = myLocation.coordinate
            marker = GMSMarker(position: myLocation.coordinate)
            marker.appearAnimation = kGMSMarkerAnimationPop
            
            marker.icon = GMSMarker.markerImageWithColor(UIColor.blueColor())
            marker.title = "Tu estás aquí :)"
            marker.flat = true
            
            marker.map = myMap
            
            locationMe = myLocation
            print(locationMe.coordinate.latitude.description)
            print(locationMe.coordinate.longitude.description)
            
            //let distance: CLLocationDistance = self.locationMe.distanceFromLocation(self.locationOut)
            //print("La distancia en metros es de \(distance.description)")

            
            myMap.settings.myLocationButton = true
            didFindMyLocation = true
            
        }
    }
    
    
    
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.AuthorizedWhenInUse {
            myMap.myLocationEnabled = true
            //locationManager.startUpdatingLocation()
            print("StartUpdatingLocation")

             myMap.settings.myLocationButton = true
        }
    }
    
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            
            print("DidUpdateLocations")
            //myMap.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
             var latestLocation: AnyObject = locations[locations.count - 1]
            if startLocation == nil {
                startLocation = latestLocation as! CLLocation
            }
            
            if didYouPutMarker {
                print("updateLocationWithArgumentDidyouputmarker")

                let distance = locationOut.distanceFromLocation(latestLocation as! CLLocation)
                //print( "\(locationMe)")
                //print( "\(locationOut)")
                //print("DISTANCE: \(distance)")
                DistanceInt = Int(distance)
                
                
                checkDistance(DistanceInt)
                //print(DistanceInt)
                
                
                
                
                
            }
            
            
 
 
            
            
            
           
        }
        
    }
    
    
    func checkDistance (distance: Int){
       
         if distance >= 200 {
                labelDistance.text = "\(distance) Metros"
                
                if nearTo200 == false {
                    notification.alertBody = "Estás muy lejos del punto objetivo \(distance) "
                    notification.alertAction = "Ok"
                    // You should set also the notification time zone otherwise the fire date is interpreted as an absolute GMT time
                    notification.timeZone = NSTimeZone.localTimeZone()
                    // you can simplify setting your fire date using dateByAddingTimeInterval
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)

                    nearTo200 = true
                    near = false
                    nearTo50 = false
                    nearTo100 = false
                    nearTo10 = false
                    labelDirection.text = "Estás muy lejos del punto objetivo "
                }
                
            }
            if distance <= 200 && distance >= 100{
                labelDistance.text = "\(distance) Metros"
                // Mandar un alert
                if nearTo100 == false {
                    notification.alertBody = "Estás lejos del punto objetivo \(distance) "
                    notification.alertAction = "Ok"
                    // You should set also the notification time zone otherwise the fire date is interpreted as an absolute GMT time
                    notification.timeZone = NSTimeZone.localTimeZone()
                    // you can simplify setting your fire date using dateByAddingTimeInterval
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)

                    nearTo100 = true
                    near = false
                    nearTo50 = false
                    nearTo200 = false
                    nearTo10 = false
                    labelDirection.text = "Estás lejos del punto objetivo"
                    
                    
                    
                }
                
                
            }
            if distance <= 100 && distance >= 50 {
                labelDistance.text = "\(distance) Metros"
                // Mandar un alert
                
                if nearTo50 == false {
                    notification.alertBody = "Estás próximo al punto objetivo \(distance) "
                    notification.alertAction = "Ok"
                    // You should set also the notification time zone otherwise the fire date is interpreted as an absolute GMT time
                    notification.timeZone = NSTimeZone.localTimeZone()
                    // you can simplify setting your fire date using dateByAddingTimeInterval
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)

                    nearTo50 = true
                    near = false
                    nearTo100 = false
                    nearTo200 = false
                    nearTo10 = false
                    labelDirection.text = "Estás próximo al punto objetivo, Distancia \(distance)"
                    
                }
                
            }
            if distance <= 50 && distance >= 10{
                labelDistance.text = "\(distance) Metros"
                
                
                // Mandar un alert
                if nearTo10 == false {
                    notification.alertBody = "Estás muy próximo al punto objetivo \(distance) "
                    notification.alertAction = "Ok"
                    // You should set also the notification time zone otherwise the fire date is interpreted as an absolute GMT time
                    notification.timeZone = NSTimeZone.localTimeZone()
                    // you can simplify setting your fire date using dateByAddingTimeInterval
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    nearTo10 = true
                    labelDirection.text = "Estás muy próximo al punto objetivo"
                    near = false
                    nearTo50 = false
                    nearTo100 = false
                    nearTo200 = false
                    
                    
                }
                
                
            }
            if distance <= 10 {
                labelDistance.text = "\(distance) Metros"
                

                if near == false {
                    notification.alertBody = "Estás en el punto objetivo \(distance) "
                    notification.alertAction = "Ok"
                    // You should set also the notification time zone otherwise the fire date is interpreted as an absolute GMT time
                    notification.timeZone = NSTimeZone.localTimeZone()
                    // you can simplify setting your fire date using dateByAddingTimeInterval
                    notification.fireDate = NSDate(timeIntervalSinceNow: 5)
                    UIApplication.sharedApplication().scheduleLocalNotification(notification)
                    near = true
                    nearTo50 = false
                    nearTo100 = false
                    nearTo200 = false
                    nearTo10 = false
                    labelDirection.text = "Estás en el punto objetivo"
                    publishOnTwitter()
                    
                }
                
            }

            
            }
            
            
            
        }
        
       
        

    




